_Docker image с MkDocs_
Образ базируется на python-alpine

В образе кроме mkdocs-material есть:

- plugin static-i18n
- plugin mkdocs-with-pdf
- plugin mkdocs-monorepo-plugin
- plugin mkdocs-git-revision-date-plugin
- plugin mkdocs-git-revision-date-localized-plugin
- plugin mkdocs-redirects
